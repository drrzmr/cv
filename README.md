Eder Ruiz | Curriculum Vitae
============================

> I’m passionate about understanding how things work and what fascinates me
> most is the world of computing. This fascination quickly led me to the world
> of Linux and Open Source which in those days meant knowing how to use the
> command line to operate the system.
>
> In terms of programing, I'm folling in love with functional paradigm, actor
> model, reactive programming, strong type system, and massive data parallel
> programming, ie: Scala, Akka and Spark
>
> Topics that also interest me are Functional Programming,
> Reactive Arquitecture, Actor Model, Big Data, High Availability,
> Fault Tolerance, Event Sourcing, Streaming Processing, CQRS, Operating
> Systems, Distributed Systems and Cloud Computing.

Education - Degree
------------------

#### 2010 - 2010

**Listener, Automation Engineering and Systems**
[Universidade Federal de Santa Catarina (Florianópolis - PR)][ufsc]
Isolated Discipline: Distributed Systems

#### 2002 - 2008

**BSc, Computer Science**
[Universidade Estadual do Oeste do Paraná (Foz do Iguaçu - PR)][unioeste]
Research Area: Computer Network and Operational Systems

[ufsc]: http://das.ufsc.br
[unioeste]: https://www.unioeste.br

Education - Courses and Training
--------------------------------

#### 2021

* [**Specialization: Functional Programming in Scala**][302]
Coursera

* [**Functional Programming in Scala Capstone**][301]
Coursera

[302]: https://www.coursera.org/account/accomplishments/specialization/certificate/4L45GQT45PA8
[301]: https://www.coursera.org/account/accomplishments/certificate/VTA6KB42UJC5

#### 2020

* [**Big Data Analysis with Scala and Spark**][207]
Coursera

* [**Parallel Programming**][206]
Coursera

* [**Fundamentals for Apache Kafka**][205]
  Confluent

* [**Functional Program Design in Scala**][204]
  Coursera

* [**Lightbend Akka Cluster - Sharding (Scala)**][203]
  Lightbend

* [**Lightbend Scala Language - Professional**][202]
  Lightbend

* [**Scala & Functional Programming for Beginners**][201]
  Udemy - Rock the JVM

[207]: https://www.coursera.org/account/accomplishments/certificate/ST8JS4LHNYHE
[206]: https://www.coursera.org/account/accomplishments/certificate/R9YDETCJFCHL
[205]: https://cloud.contentraven.com/crdownloadfiles/index.aspx?act=8MH4sSWyowEaN0eXRAsy+A__&id=rHMoknx/ECM_
[204]: https://coursera.org/share/0b2d7a40d50e68547bcd073c8a82295e
[203]: https://academy.lightbend.com/certificates/df101313fb28443a89f230a65664a4fc
[202]: https://academy.lightbend.com/certificates/6f830e5eaa9946559e81d92044dcf2bb
[201]: https://www.udemy.com/certificate/UC-OLOEHGQU/

#### 2019

* [**Advanced Scala and Functional Programming**][196]
  Udemy - Rock the JVM

* [**Reactive Architecture: CQRS & Event Sourcing**][195]
  Lightbend on Cognitive Class

* [**LFS158x: Introduction to Kubernetes**][194]
  edX

* [**Akka HTTP with Scala**][193]
  Udemy - Rock the JVM

* [**scala-reactiveX: Programming Reactive Systems**][192]
  edX

* [**Functional Programming Principles in Scala**][191]
  Coursera

[196]: https://www.udemy.com/certificate/UC-HBRULHPG/
[195]: https://courses.cognitiveclass.ai/certificates/a0618a474bb84fb4a5e816e2bdbb3423
[194]: https://courses.edx.org/certificates/d67a0c096db04db6a969be0ca0783215
[193]: https://www.udemy.com/certificate/UC-83TGZRP4/
[192]: https://courses.edx.org/certificates/e1fac0ae6467404cbe0347cfcf5918d1
[191]: https://www.coursera.org/account/accomplishments/verify/BP2G726T2FEF

#### 2018

* [**Reactive Architecture: Reactive Microservices**][185]
  Lightbend on Cognitive Class

* [**Reactive Architecture: Domain Driven Design**][184]
  Lightbend on Cognitive Class

* [**Reactive Architecture: Introduction to Reactive Systems**][183]
  Lightbend on Cognitive Class

* [**Scala 101**][182]
  Cognitive Class

* [**Introduction to Big Data**][181]
  Coursera

[185]: https://courses.cognitiveclass.ai/certificates/dd2755aea4eb4d1c941a8d6453f1cc04
[184]: https://courses.cognitiveclass.ai/certificates/131752377e294106905365fa92b4e43a
[183]: https://courses.cognitiveclass.ai/certificates/44657c6564374ef68abc2eeaa9895e7e
[182]: https://courses.cognitiveclass.ai/certificates/6d8c1a09742d41209e5579d31a884f32
[181]: https://www.coursera.org/account/accomplishments/verify/VW27JH8FX9WX

#### 2017

* [**Getting Started with Apache Kafka**][175]
  Pluralsight

* [**Administering an Elasticsearch Cluster**][174]
  Pluralsight

* [**Centralized Logging with the Elastic Stack**][173]
  Pluralsight

* [**The Go Programming Language Guide: Code Like a Pro**][172]
  Udemy

* [**Learn How To Code: Google's Go (golang) Programming Language**][171]
  Udemy

[175]: https://www.pluralsight.com/courses/apache-kafka-getting-started
[174]: https://www.pluralsight.com/courses/administering-elasticsearch-cluster
[173]: https://www.pluralsight.com/courses/centralized-logging-elastic-stack
[172]: https://www.udemy.com/certificate/UC-CZ9ZEO54
[171]: https://www.udemy.com/certificate/UC-M1RT5B57

#### 2016

* [**Go Fundamentals**][162]
  Pluralsight

* [**Go: Getting Started**][161]
   Pluralsight

[162]: https://www.pluralsight.com/courses/go-fundamentals
[161]: https://www.pluralsight.com/courses/go-getting-started

#### 2015

* [**Big Data, Cloud Computing & CDN Emerging Technologies**][156]
  Coursera

* [**An Introduction to Interactive Programming in Python (Part 1)**][155]
   Coursera

* [**Programming for Everybody (Python)**][154]
   Coursera

* [**R Programming**][153]
  Coursera

* [**The Data Scientist’s Toolbox**][152]
  Coursera

* [**From Nand to Tetris - Part I**][151]
  Coursera

* **Parallel Computing with OpenCL for Altera FPGAs**
  Institute Nokia of Technology

* **Pattern Recognition**
  Institute Nokia of Technology

* **Computational Photography**
  Institute Nokia of Technology

* **Computer Vision**
  Institute Nokia of Technology

[156]: https://www.coursera.org/account/accomplishments/verify/X5WMY5X54SN4
[155]: https://www.coursera.org/account/accomplishments/records/vNyvr87VvhPZFS3P
[154]: https://www.coursera.org/account/accomplishments/records/mMPAEddsH9QqH6mG
[153]: https://www.coursera.org/account/accomplishments/records/HffZUfPQujypyNhG
[152]: https://www.coursera.org/account/accomplishments/certificate/228MEG7NXR
[151]: https://www.coursera.org/account/accomplishments/records/Z82U4QSdGJxSJRzC

#### 2014

* **Digital Image Processing**
  Institute Nokia of Technology

* **Digital Signal Processing**
  Institute Nokia of Technology

* **CUDA Programming**
  Institute Nokia of Technology

Experience - Professional
-------------------------

#### [Linx - Linx Impulse][linx]

Big Data Architect | 2021/01 - current

##### Projects
```json
{
  "Data Platform": "Architecture Evolution and Improvements"
}
```

#### [Creditas][creditas]

Specialist Software Engineer (Big Data) | 2020/03 - 2020/12

##### Projects
```json
{
  "Data Platform": [
    "Develop Apache Kafka cluster migration tooling",
    "Coordinate and act on the migration of 280+ kafka topics",
    "Fine tuning Hadoop cluster for Spark jobs (batch)",
    "Near real-time data processing (streaming)"
  ]
}
```
##### Tools and Methodologies
```json
{
  "BigData": [
    "Apache Kafka",
    "Apache Kafka Connect",
    "Confluent Schema Registry",
    "Apache Zookeeper",
    "Apache Hadoop (AWS EMR)",
	"Apache Spark"
  ],
  "Languages": [ "Scala", "Java", "Python", "Shell Script" ],
  "Cloud": [ "Amazon Web Services" ],
  "DevOps": [ "Docker", "Terraform" ],
  "Version Control": [ "git", "github", "circleci" ]
}
```

#### [Linx - Linx Impulse][linx]

Specialist Software Developer (Big Data) | 2018/11 - 2020/02

##### Projects
```json
{
  "Platform": [
    "Keep http api with 500k request per minute (and growing), up running",
    "Fine tunning to cost efective and higt performance infraestructure"
  ],
  "Schema": [
    "Reduce usage of JSON in favor of Avro and ORC"
  ]
}
```

##### Tools and Methodologies
```json
{
  "BigData": [
    "Apache Kafka",
    "Apache Spark",
    "Apache Zookeeper",
    "Apache Storm",
    "Apache Hadoop",
    "Apache Hive",
    "Apache Cassandra",
    "Elasticsearch",
    "Pinterest Secor",
    "Amazon DynamoDB"
  ],
  "Languages": [ "Scala", "Java", "Python", "Ruby", "Shell Script" ],
  "Framework": [ "Akka", "Vert.x", "Apache Avro", "Apache ORC" ],
  "Cloud": [ "Amazon Web Services" ],
  "Monitoring": [ "Prometheus", "Grafana", "Amazon Cloudwatch" ],
  "DevOps": [ "Docker", "Ansible", "Terraform", "Chef" ],
  "Version Control": [ "git", "github", "circleci" ]
}
```

#### [Neoway Business Solutions][neoway]

Senior Software Developer (Data Engineer) | 2018/02 - 2018/10

##### Projects
```json
{
  "Data Lake and Streaming Platform": "Initial pilot project",
  "DataOps": "Help analytics team automating and improving processes"
}
```

##### Tools and Methodologies
```json
{
  "BigData":  [ "Apache Hadoop", "Apache Tez", "Apache Hive" ],
  "Languages": [ "Python", "Scala" ],
  "Cloud": [ "Amazon Web Services", "Microsoft Azure" ],
  "DevOps": [ "Docker", "Ansible", "Terraform", "Packer" ],
  "Version Control": [ "git", "gitlab", "gitlab-ci-cd" ]
}
```

#### [Pagar.me Pagamentos][pagarme]

Senior Software Developer (Backend) | 2017/02 - 2018/01

##### Projects
```json
{
  "Golang Backend Development":  [
    "https://golang.org",
    "https://github.com/kataras/iris",
    "https://www.vaultproject.io",
    "https://www.terraform.io",
    "http://cassandra.apache.org"
  ]
}
```

##### Tools and Methodologies
```json
{
  "Languages": [ "Go" ],
  "Cloud": [ "Amazon Web Services" ],
  "DevOps": [ "Docker", "Ansible", "Consul", "Vagrant" ],
  "Version Control": [ "git", "github", "travis-ci" ]
}
```

#### [Axiros Lasting Advantage][axiros]

Senior Software Engineer (Backend) | 2016/05 - 2017/02

##### **Projects**
```json
{
  "Python Backend Development":  [
    "http://python.org",
    "http://github.com/zopefoundation/Zope",
    "http://python-rq.org",
    "https://dev.mysql.com",
    "https://www.mongodb.com"
  ],
  "Javascript Frontend Development":  [
    "https://angularjs.org",
    "https://jquery.com",
    "http://python-rq.org",
    "https://dev.mysql.com",
    "https://www.mongodb.com"
  ],
  "Protocol Gateway": [ "telnet", "ssh", "tr-069", "rest", "soap" ]
}
```

##### Tools and Methodologies
```json
{
  "Languages": [ "Python", "JavaScript" ],
  "DevOps": [ "Docker", "Vagrant" ],
  "Version Control": [ "git", "hg", "svn" ]
}
```

#### [Neoway Business Solutions][neoway]

Senior Software Developer (Backend) | 2015/09 - 2016/05

##### Projects
```json
{
  "Python Backend Development": [
    "http://flask.pocoo.org",
    "https://twistedmatrix.com",
    "https://scrapy.org",
    "https://github.com/boto/boto3"
  ],
  "Computer Vision and Machine Learning": [
    "https://caffe.berkeleyvision.org",
    "https://python-pillow.org",
    "https://www.numpy.org",
    "https://matplotlib.org"
  ],
  "Hight Availiability and Scalability Clustering": [
    "https://www.haproxy.org",
    "https://coreos.com/etcd",
    "https://github.com/kelseyhightower/confd"
  ]
}
```

##### Tools and Methodologies
```json
{
  "Languages": [ "Python", "Bash Script" ],
  "Cloud": [ "Amazon Web Services" ],
  "DevOps": [ "Docker", "Ansible" ],
  "Version Control": [ "git", "gitlab", "gitlab-ci-cd" ]
}
```

#### [Instituto Nokia of Tecnologia - INdT][indt]

Full Software Developer | 2012/06 - 2015/06

##### Projects
```json
{
  "Bluetooth Low Energy": [
    "Bluez Commiter",
    "Android Native Development (prototyping)",
    "Arduino (prototyping)"
    "Full Stack Development (prototyping)",
    "Qt Demos"
  ],
  "Nokia Platform": [
    "Android (prototyping)",
    "Computer Vision on FPGA (prototying)",
    "Qt Desktop and Mobile"
  ]
}
```

##### Tools and Methodologies
```json
{
  "Scrum": [ "icescrum", "targetprocess" ],
  "Version Control": [ "git", "gerrit", "phabricator" ]
}
```

#### [Ahgora Sistemas][ahgora]

Software Developer | 2012/01 - 2012/06

##### Projects
```json
{
  "Linux Embedded Development": [
    "Lua and C embedded application",
    "Device Driver (atmel at91sam9g20)"
  ],
  "Firmware Development": [
    "Capacitive Keyboard (quantum qt60248)",
    "ARM Cortex-M3 (nxp lpc1768)",
    "Ultra low-power 8051 (nordic nrf24le1)"
  ]
}
```

#### [Dígitro Tecnologia][digitro]

Systems Analyst | 2009/11 - 2011/12

##### Projects
```json
{
  "Linux Embedded Development": [
    "Glib embedded application",
    "Linux Device Driver (vortex86mx, omap5912)",
    "uCLinux Device Driver (blackfin - uart, spi, i2c, memory mapped io, dma)"
  ],
  "Firmware Development": [
    "Freescale HCS08 - uart, pwm, gpio"
  ]
}
```

#### [Instituto de Tecnologia Aplicada e Inovação - ITAI][itai]

Systems Analyst | 2009/02 - 2009/10

##### Projects
```json
{
  "Digital Fault Recorder": [
    "Linux device driver for data acquisition using comedi framework",
    "Linux hard real time tasks using RTAI and Xenomai frameworks"
  ],
  "Electric Vehicle": [
    "Linux application (java jni, gps garmin 18x usb, modbus rtu, uart)"
  ]
}
```

[itai]: http://itai.org.br
[digitro]: https://www.digitro.com
[ahgora]: https://www.ahgora.com.br
[indt]: http://www.indt.org.br
[axiros]: https://www.axiros.com
[pagarme]: https://pagar.me
[neoway]: https://www.neoway.com.br
[linx]: https://linximpulse.com.br
[creditas]: https://www.creditas.com/

Experience - Internships
------------------------

#### Instituto de Tecnologia Aplicada e Inovação - ITAI

* 2008/06 - 2008/12
* System administration (Linux)

#### Centro Internacional de Hidroinformatica - CIH

* 2008/01 - 2008/06
* LAMP web development 

#### B3 Informática

* 2008/02 - 2008/04
* LAMP web development

#### Parque Tecnológico Itaipu - PTI

* 2007/03 - 2007/05
* Reserch on access control and industrial network protocols

#### Prognus Software Livre

* 2006/03 - 2006/10
* Linux firewall performance test and training material development

#### Kionux Soluções em Internet

* 2003/11 - 2004/11
* System administration (Linux)

#### Instituto de Tecnologia Aplicada e Inovação - ITAI

* 2002/11 - 2003/11
* Research on network operation systems, linux, tcp/ip


Languages
---------

Portuguese: **Native**

English: **Intermediate**

---

> drrzmr@gmail.com • +55 (48) 9-9169-0122 • 1982/11/30
>
> https://github.com/drrzmr
>
> https://www.linkedin.com/in/drrzmr
