Eder Ruiz | Curriculum Vitae
============================

> Tenho paixão por entender como as coisas funcionam e o que mais me fascina
> é o universo da computação. Esse fascínio me levou rapidamente ao mundo de
> Linux e Open Source, que naquela época significava saber como usar a
> linha de comando para utilizar o sistema.
>
> Em termos de programação, sou apaixonado pelo paradigma funcional, modelo de
> atores, programação reativa, sistema de tipagem forte e processamento paralelo
> massivo de dados, ou seja: Scala, Akka e Spark
>
> Tópicos que também me interessam são Programação Funcional,
> Arquitetura Reativa, Modelo de Atores, Big Data, Alta Disponibilidade,
> Tolerância a Falhas, Event Sourcing, Streaming Processing, CQRS, Sistemas
> Operacionais, Sistemas Distribuídos e Cloud Computing. 

Educação - Graduação
--------------------

#### 2010 - 2010

**Ouvinte, Engenharia de Automação e Sistemas**
[Universidade Federal de Santa Catarina (Florianópolis - PR)][ufsc]
Disciplina Isolada: Sistemas Distribuidos

#### 2002 - 2008

**Bacharelado, Ciência da Computação**
[Universidade Estadual do Oeste do Paraná (Foz do Iguaçu - PR)][unioeste]
Área de Pesquisa: Redes de Computadores e Sistemas Operacionais

[ufsc]: http://das.ufsc.br
[unioeste]: https://www.unioeste.br

Educação - Cursos e Treinamentos
--------------------------------

#### 2021

* [**Specialization: Functional Programming in Scala**][302]
Coursera

* [**Functional Programming in Scala Capstone**][301]
Coursera

[302]: https://www.coursera.org/account/accomplishments/specialization/certificate/4L45GQT45PA8
[301]: https://www.coursera.org/account/accomplishments/certificate/VTA6KB42UJC5

#### 2020

* [**Big Data Analysis with Scala and Spark**][207]
Coursera

* [**Parallel Programming**][206]
Coursera

* [**Fundamentals for Apache Kafka**][205]
  Confluent

* [**Functional Program Design in Scala**][204]
  Coursera

* [**Lightbend Akka Cluster - Sharding (Scala)**][203]
  Lightbend

* [**Lightbend Scala Language - Professional**][202]
  Lightbend

* [**Scala & Functional Programming for Beginners**][201]
  Udemy - Rock the JVM

[207]: https://www.coursera.org/account/accomplishments/certificate/ST8JS4LHNYHE
[206]: https://www.coursera.org/account/accomplishments/certificate/R9YDETCJFCHL
[205]: https://cloud.contentraven.com/crdownloadfiles/index.aspx?act=8MH4sSWyowEaN0eXRAsy+A__&id=rHMoknx/ECM_
[204]: https://coursera.org/share/0b2d7a40d50e68547bcd073c8a82295e
[203]: https://academy.lightbend.com/certificates/df101313fb28443a89f230a65664a4fc
[202]: https://academy.lightbend.com/certificates/6f830e5eaa9946559e81d92044dcf2bb
[201]: https://www.udemy.com/certificate/UC-OLOEHGQU/

#### 2019

* [**Advanced Scala and Functional Programming**][196]
  Udemy - Rock the JVM

* [**Reactive Architecture: CQRS & Event Sourcing**][195]
  Lightbend on Cognitive Class

* [**LFS158x: Introduction to Kubernetes**][194]
  edX

* [**Akka HTTP with Scala**][193]
  Udemy - Rock the JVM

* [**scala-reactiveX: Programming Reactive Systems**][192]
  edX

* [**Functional Programming Principles in Scala**][191]
  Coursera

[196]: https://www.udemy.com/certificate/UC-HBRULHPG/
[195]: https://courses.cognitiveclass.ai/certificates/a0618a474bb84fb4a5e816e2bdbb3423
[194]: https://courses.edx.org/certificates/d67a0c096db04db6a969be0ca0783215
[193]: https://www.udemy.com/certificate/UC-83TGZRP4/
[192]: https://courses.edx.org/certificates/e1fac0ae6467404cbe0347cfcf5918d1
[191]: https://www.coursera.org/account/accomplishments/verify/BP2G726T2FEF

#### 2018

* [**Reactive Architecture: Reactive Microservices**][185]
  Lightbend on Cognitive Class

* [**Reactive Architecture: Domain Driven Design**][184]
  Lightbend on Cognitive Class

* [**Reactive Architecture: Introduction to Reactive Systems**][183]
  Lightbend on Cognitive Class

* [**Scala 101**][182]
  Cognitive Class

* [**Introduction to Big Data**][181]
  Coursera

[185]: https://courses.cognitiveclass.ai/certificates/dd2755aea4eb4d1c941a8d6453f1cc04
[184]: https://courses.cognitiveclass.ai/certificates/131752377e294106905365fa92b4e43a
[183]: https://courses.cognitiveclass.ai/certificates/44657c6564374ef68abc2eeaa9895e7e
[182]: https://courses.cognitiveclass.ai/certificates/6d8c1a09742d41209e5579d31a884f32
[181]: https://www.coursera.org/account/accomplishments/verify/VW27JH8FX9WX

#### 2017

* [**Getting Started with Apache Kafka**][175]
  Pluralsight

* [**Administering an Elasticsearch Cluster**][174]
  Pluralsight

* [**Centralized Logging with the Elastic Stack**][173]
  Pluralsight

* [**The Go Programming Language Guide: Code Like a Pro**][172]
  Udemy

* [**Learn How To Code: Google's Go (golang) Programming Language**][171]
  Udemy

[175]: https://www.pluralsight.com/courses/apache-kafka-getting-started
[174]: https://www.pluralsight.com/courses/administering-elasticsearch-cluster
[173]: https://www.pluralsight.com/courses/centralized-logging-elastic-stack
[172]: https://www.udemy.com/certificate/UC-CZ9ZEO54
[171]: https://www.udemy.com/certificate/UC-M1RT5B57

#### 2016

* [**Go Fundamentals**][162]
  Pluralsight

* [**Go: Getting Started**][161]
   Pluralsight

[162]: https://www.pluralsight.com/courses/go-fundamentals
[161]: https://www.pluralsight.com/courses/go-getting-started

#### 2015

* [**Big Data, Cloud Computing & CDN Emerging Technologies**][156]
  Coursera

* [**An Introduction to Interactive Programming in Python (Part 1)**][155]
   Coursera

* [**Programming for Everybody (Python)**][154]
   Coursera

* [**R Programming**][153]
  Coursera

* [**The Data Scientist’s Toolbox**][152]
  Coursera

* [**From Nand to Tetris - Part I**][151]
  Coursera

* **Parallel Computing with OpenCL for Altera FPGAs**
  Institute Nokia of Technology

* **Pattern Recognition**
  Institute Nokia of Technology

* **Computational Photography**
  Institute Nokia of Technology

* **Computer Vision**
  Institute Nokia of Technology

[156]: https://www.coursera.org/account/accomplishments/verify/X5WMY5X54SN4
[155]: https://www.coursera.org/account/accomplishments/records/vNyvr87VvhPZFS3P
[154]: https://www.coursera.org/account/accomplishments/records/mMPAEddsH9QqH6mG
[153]: https://www.coursera.org/account/accomplishments/records/HffZUfPQujypyNhG
[152]: https://www.coursera.org/account/accomplishments/certificate/228MEG7NXR
[151]: https://www.coursera.org/account/accomplishments/records/Z82U4QSdGJxSJRzC

#### 2014

* **Digital Image Processing**
  Institute Nokia of Technology

* **Digital Signal Processing**
  Institute Nokia of Technology

* **CUDA Programming**
  Institute Nokia of Technology

Experiência - Professional
-------------------------

#### [Linx - Linx Impulse][linx]

Arquiteto de Big Data | 2021/01 - atual

##### Projetos
```json
{
  "Data Platform": "Evolução arquitetural"
}
```

#### [Creditas][creditas]

Engenheiro de Dados - Especialista Big Data | 2020/03 - 2020/12

##### Projetos
```json
{
  "Data Platform": [
    "Desenvolvimento de ferramenta para auxilio na migração de cluster Apache Kafka",
    "Coordenar e atuar na migração de 280+ tópicos kafka",
    "Fine tuning de cluster Hadoop para jobs Spark (batch)",
    "Processamento de dados near real-time (streaming)"
  ]
}
```
##### Ferramentas e Metodologias
```json
{
  "BigData": [
    "Apache Kafka",
    "Apache Kafka Connect",
    "Confluent Schema Registry",
    "Apache Zookeeper",
    "Apache Hadoop (AWS EMR)",
	"Apache Spark"
  ],
  "Linguagens": [ "Scala", "Java", "Python", "Shell Script" ],
  "Cloud": [ "Amazon Web Services" ],
  "DevOps": [ "Docker", "Terraform" ],
  "Version Control": [ "git", "github", "circleci" ]
}
```

#### [Linx - Linx Impulse][linx]

Desenvolvedor de Software - Especialista Big Data | 2018/11 - 2020/02

##### Projetos
```json
{
  "Platform": [
    "Manter api http api com 500k requisições por minuto (e crescendo)",
    "Fine tuning de infraestrutura para custo efetivo de alta performance"
  ],
  "Schema": [
    "Reduzir uso de JSON utilizando Avro e ORC"
  ]
}
```

##### Ferramentas e Metodologias
```json
{
  "BigData": [
    "Apache Kafka",
    "Apache Spark",
    "Apache Zookeeper",
    "Apache Storm",
    "Apache Hadoop",
    "Apache Hive",
    "Apache Cassandra",
    "Elasticsearch",
    "Pinterest Secor",
    "Amazon DynamoDB"
  ],
  "Linguagens": [ "Scala", "Java", "Python", "Ruby", "Shell Script" ],
  "Framework": [ "Akka", "Vert.x", "Apache Avro", "Apache ORC" ],
  "Cloud": [ "Amazon Web Services" ],
  "Monitoramento": [ "Prometheus", "Grafana", "Amazon Cloudwatch" ],
  "DevOps": [ "Docker", "Ansible", "Terraform", "Chef" ],
  "Version Control": [ "git", "github", "circleci" ]
}
```

#### [Neoway Business Solutions][neoway]

Software Developer - Engenheiro de Dados Senior | 2018/02 - 2018/10

##### Projetos
```json
{
  "Data Lake e Plataforma de Streaming": "Projeto piloto inicial",
  "DataOps": "Auxilio ao time analítico em processos de automação"
}
```

##### Ferramentas e Metodologias
```json
{
  "BigData":  [ "Apache Hadoop", "Apache Tez", "Apache Hive" ],
  "Linguagens": [ "Python", "Scala" ],
  "Cloud": [ "Amazon Web Services", "Microsoft Azure" ],
  "DevOps": [ "Docker", "Ansible", "Terraform", "Packer" ],
  "Version Control": [ "git", "gitlab", "gitlab-ci-cd" ]
}
```

#### [Pagar.me Pagamentos][pagarme]

Software Developer - Backend, Senior | 2017/02 - 2018/01

##### Projetos
```json
{
  "Golang Backend Development":  [
    "https://golang.org",
    "https://github.com/kataras/iris",
    "https://www.vaultproject.io",
    "https://www.terraform.io",
    "http://cassandra.apache.org"
  ]
}
```

##### Ferramentas e Metodologias
```json
{
  "Linguagens": [ "Golang" ],
  "Cloud": [ "Amazon Web Services" ],
  "DevOps": [ "Docker", "Ansible", "Consul", "Vagrant" ],
  "Version Control": [ "git", "github", "travis-ci" ]
}
```

#### [Axiros Lasting Advantage][axiros]

Engenheiro de Software - Backend, Senior | 2016/05 - 2017/02

##### **Projetos**
```json
{
  "Python Backend Development":  [
    "http://python.org",
    "http://github.com/zopefoundation/Zope",
    "http://python-rq.org",
    "https://dev.mysql.com",
    "https://www.mongodb.com"
  ],
  "Javascript Frontend Development":  [
    "https://angularjs.org",
    "https://jquery.com",
    "http://python-rq.org",
    "https://dev.mysql.com",
    "https://www.mongodb.com"
  ],
  "Protocol Gateway": [ "telnet", "ssh", "tr-069", "rest", "soap" ]
}
```

##### Ferramentas e Metodologias
```json
{
  "Languages": [ "Python", "JavaScript" ],
  "DevOps": [ "Docker", "Vagrant" ],
  "Version Control": [ "git", "hg", "svn" ]
}
```

#### [Neoway Business Solutions][neoway]

Senior Software Developer (Backend) | 2015/09 - 2016/05

##### Projetos
```json
{
  "Python Backend Development": [
    "http://flask.pocoo.org",
    "https://twistedmatrix.com",
    "https://scrapy.org",
    "https://github.com/boto/boto3"
  ],
  "Computer Vision and Machine Learning": [
    "https://caffe.berkeleyvision.org",
    "https://python-pillow.org",
    "https://www.numpy.org",
    "https://matplotlib.org"
  ],
  "Hight Availiability and Scalability Clustering": [
    "https://www.haproxy.org",
    "https://coreos.com/etcd",
    "https://github.com/kelseyhightower/confd"
  ]
}
```

##### Ferramentas e Metodologias
```json
{
  "Languages": [ "Python", "Bash Script" ],
  "Cloud": [ "Amazon Web Services" ],
  "DevOps": [ "Docker", "Ansible" ],
  "Version Control": [ "git", "gitlab", "gitlab-ci-cd" ]
}
```

#### [Instituto Nokia of Tecnologia - INdT][indt]

Full Software Developer | 2012/06 - 2015/06

##### Projetos
```json
{
  "Bluetooth Low Energy": [
    "Bluez Commiter",
    "Android Native Development (prototyping)",
    "Arduino (prototyping)"
    "Full Stack Development (prototyping)",
    "Qt Demos"
  ],
  "Nokia Platform": [
    "Android (prototyping)",
    "Computer Vision on FPGA (prototying)",
    "Qt Desktop and Mobile"
  ]
}
```

##### Ferramentas e Metodologias
```json
{
  "Scrum": [ "icescrum", "targetprocess" ],
  "Version Control": [ "git", "gerrit", "phabricator" ]
}
```

#### [Ahgora Sistemas][ahgora]

Software Developer | 2012/01 - 2012/06

##### Projetos
```json
{
  "Linux Embedded Development": [
    "Lua and C embedded application",
    "Device Driver (atmel at91sam9g20)"
  ],
  "Firmware Development": [
    "Capacitive Keyboard (quantum qt60248)",
    "ARM Cortex-M3 (nxp lpc1768)",
    "Ultra low-power 8051 (nordic nrf24le1)"
  ]
}
```

#### [Dígitro Tecnologia][digitro]

Systems Analyst | 2009/11 - 2011/12

##### Projetos
```json
{
  "Linux Embedded Development": [
    "Glib embedded application",
    "Linux Device Driver (vortex86mx, omap5912)",
    "uCLinux Device Driver (blackfin - uart, spi, i2c, memory mapped io, dma)"
  ],
  "Firmware Development": [
    "Freescale HCS08 - uart, pwm, gpio"
  ]
}
```

#### [Instituto de Tecnologia Aplicada e Inovação - ITAI][itai]

Systems Analyst | 2009/02 - 2009/10

##### Projetos
```json
{
  "Digital Fault Recorder": [
    "Linux device driver for data acquisition using comedi framework",
    "Linux hard real time tasks using RTAI and Xenomai frameworks"
  ],
  "Electric Vehicle": [
    "Linux application (java jni, gps garmin 18x usb, modbus rtu, uart)"
  ]
}
```

[itai]: http://itai.org.br
[digitro]: https://www.digitro.com
[ahgora]: https://www.ahgora.com.br
[indt]: http://www.indt.org.br
[axiros]: https://www.axiros.com
[pagarme]: https://pagar.me
[neoway]: https://www.neoway.com.br
[linx]: https://linximpulse.com.br
[creditas]: https://www.creditas.com/

Experiência - Estágio
------------------------

#### Instituto de Tecnologia Aplicada e Inovação - ITAI

* 2008/06 - 2008/12
* Administação de Sistemas (Linux)

#### Centro Internacional de Hidroinformatica - CIH

* 2008/01 - 2008/06
* Desenvolvimento WEB - LAMP

#### B3 Informática

* 2008/02 - 2008/04
* Desenvolvimento WEB - LAMP

#### Parque Tecnológico Itaipu - PTI

* 2007/03 - 2007/05
* Pesquisa sobre controle de acesso de protocolos de rede industriais

#### Prognus Software Livre

* 2006/03 - 2006/10
* Teste de performance de firewall linux
* Desenvolvimento de material de treinamento

#### Kionux Soluções em Internet

* 2003/11 - 2004/11
* Administração de Sistemas (Linux)

#### Instituto de Tecnologia Aplicada e Inovação - ITAI

* 2002/11 - 2003/11
* Pesquisa sobre redes de computadores e sistemas operacionais, linux, tcp/ip


Linguagens
----------

Português: **Nativo**

Inglês: **Intermediário**

---

> drrzmr@gmail.com • +55 (48) 9-9169-0122 • 1982/11/30
>
> https://github.com/drrzmr
>
> https://www.linkedin.com/in/drrzmr
